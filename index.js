require('dotenv').config();

const puppeteer = require('puppeteer');
const fs = require('fs');

const USERNAME = process.env.DRCHRONO_USERNAME;
const PASSWORD = process.env.DRCHRONO_PASSWORD;
const LOGIN_URL = 'https://app.drchrono.com/accounts/login/';
const PATIENT_DETAIL_URL = 'https://provider123.drchrono.com/patients/';

(async () => {
	const browser = await puppeteer.launch({ headless: false });
	const page = await browser.newPage();

	await page.goto(LOGIN_URL);

	await page.waitForSelector('input[id=username]');
	await page.type('input[id=username]', USERNAME);
	await page.type('input[id=password]', PASSWORD);


	await Promise.all([
		page.click('input[type=submit]'),
		page.waitForNavigation({ waitUntil: 'networkidle2' })
	]);

    await Promise.all([
        page.goto('https://practicegroup.drchrono.com/patients/?page=1&query=Adams'),
        page.waitForNavigation({ waitUntil: 'networkidle2' })
    ]);

    
    const patients = await page.evaluate(() => {
    const rowArray = Array.from(document.querySelectorAll('#id_patient_list tr'));
    return rowArray.slice(1).map(tr => {
        const dataNodeList = tr.querySelectorAll('td');
        const dataArray = Array.from(dataNodeList);
        const [ id, provider, lastName, firstName, phone, mobile, lastApt, nextapt, followUp, validIns ] = dataArray.map(td => td.innerHTML);
    
        return {
            id:id.split('/')[2], 
            provider, 
            lastName, 
            firstName, 
            phone, 
            mobile, 
            lastApt, 
            nextapt, 
            followUp,
            validIns:validIns.includes('ok') ? true : false
        };
      });
    });

    console.log('patients', patients);
    
    await Promise.all([
        page.goto(PATIENT_DETAIL_URL.concat(patients[0].id)),
        page.waitForNavigation({ waitUntil: 'networkidle2' })
    ]);

    await page.waitForSelector('input[name=title]');
	await page.type('input[name=title]', 'TEST TITLE');
	
	// await browser.close();
})();
